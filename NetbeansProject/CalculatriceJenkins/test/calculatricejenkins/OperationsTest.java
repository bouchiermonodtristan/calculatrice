/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatricejenkins;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bouch
 */
public class OperationsTest {
    
    public OperationsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of division method, of class Operations.
     */
    @Test
    public void testDivision() {
        System.out.println("division");
        float a = 2.0F;
        float b = 2.0F;
        Operations instance = new Operations();
        float expResult = 1.0F;
        float result = instance.division(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of multiplication method, of class Operations.
     */
    @Test
    public void testMultiplication() {
        System.out.println("multiplication");
        float a = 2.0F;
        float b = 2.0F;
        Operations instance = new Operations();
        float expResult = 4.0F;
        float result = instance.multiplication(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of addition method, of class Operations.
     */
    @Test
    public void testAddition() {
        System.out.println("addition");
        float a = 2.0F;
        float b = 2.0F;
        Operations instance = new Operations();
        float expResult = 4.0F;
        float result = instance.addition(a, b);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of soustraction method, of class Operations.
     */
    @Test
    public void testSoustraction() {
        System.out.println("soustraction");
        float a = 3.0F;
        float b = 2.0F;
        Operations instance = new Operations();
        float expResult = 1.0F;
        float result = instance.soustraction(a, b);
        assertEquals(expResult, result, 0.0);
    }
    
}
